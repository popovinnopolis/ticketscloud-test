from django.apps import AppConfig


class RestkeysConfig(AppConfig):
    name = 'restkeys'
