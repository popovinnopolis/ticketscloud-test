from django.db import models


class Keys(models.Model):
    NOT_ISSUED_STATUS = 0
    ISSUED_STATUS = 1
    SPENT_STATUS = 2
    STATUS_CHOICES = (
        (NOT_ISSUED_STATUS, 'Not issued'),
        (ISSUED_STATUS, 'Issued'),
        (SPENT_STATUS, 'Spent'),
    )
    kkey = models.IntegerField(null=False, primary_key=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=NOT_ISSUED_STATUS)
