from django.urls import path, re_path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('key/', csrf_exempt(views.KeyView.as_view()), name='key_view'),
    re_path('key/(?P<s_key>[a-zA-Z0-9]{4})/', csrf_exempt(views.KeyView.as_view()), name='key_view'),
    path('status/', csrf_exempt(views.StatusView.as_view()), name='status_view'),
    path('generate/', csrf_exempt(views.GenerateView.as_view()), name='generate_view'),
]