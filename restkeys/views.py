import json

from django.db import connection
from django.http import HttpResponse
from django.views import View

from keygen import BASE, int2key, key2int, LEN
from restkeys.models import Keys


class KeyView(View):
    def get(self, request, s_key=None):
        """Get new key"""
        obj_key = Keys.objects.filter(status=Keys.NOT_ISSUED_STATUS).first()
        if not obj_key:
            return HttpResponse(json.dumps({"Operation": "Get new key", "status": 'No keys'}),
                                content_type='application/json')
        obj_key.status = Keys.ISSUED_STATUS
        obj_key.save()
        s_key = int2key(obj_key.kkey)
        return HttpResponse(json.dumps({"Operation": "Get new key", "status": 'Ok', 'key': s_key}),
                            content_type='application/json')

    def post(self, request, s_key=None):
        """Redeem key"""
        obj_key = Keys.objects.filter(kkey=key2int(s_key), status=Keys.ISSUED_STATUS).first()
        if not obj_key:
            return HttpResponse(json.dumps({"Operation": "Redeem key", "status": 'Key Error', 'key': s_key}),
                                content_type='application/json')

        obj_key.status = Keys.SPENT_STATUS
        obj_key.save()
        return HttpResponse(
            json.dumps({"Operation": "Redeem key", "status": 'Ok', 'key status': obj_key.get_status_display()}),
            content_type='application/json')

    def put(self, request, s_key=None):
        """Get status of the key"""
        obj_key = Keys.objects.filter(kkey=key2int(s_key)).first()
        if not obj_key:
            return HttpResponse(json.dumps({"Operation": "Get status of key", "status": 'Key Error', 'key': s_key}),
                                content_type='application/json')
        return HttpResponse(
            json.dumps({"Operation": "Get status of key", "status": 'Ok', 'key status': obj_key.get_status_display()}),
            content_type='application/json')


class StatusView(View):
    def get(self, request):
        """Get count NOT ISSUED keys"""
        count_keys = Keys.objects.filter(status=Keys.NOT_ISSUED_STATUS).count()
        return HttpResponse(json.dumps({"Operation": "Status keys", "status": 'Ok', 'remaining keys': count_keys}),
                            content_type='application/json')


class GenerateView(View):
    def post(self, request):
        """Generate keys"""
        count_keys = Keys.objects.count()
        if count_keys:
            return HttpResponse(json.dumps({"Operation": "Generate keys", "status": 'The keys already generated'}),
                                content_type='application/json')

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO restkeys_keys SELECT *, 0 FROM generate_series(0, %s)', [BASE ** LEN - 1])

        # batch_size = 100000
        # objs = (Keys(kkey=x) for x in range(BASE ** 4 - 1))
        # while True:
        #     batch = list(islice(objs, batch_size))
        #     if not batch:
        #         break
        #     Keys.objects.bulk_create(batch, batch_size)
        return HttpResponse(json.dumps({"Operation": "Generate keys", "status": 'Ok'}), content_type='application/json')
