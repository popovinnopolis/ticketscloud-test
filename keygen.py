import string

LEN = 4

CHRS = string.digits + string.ascii_letters
BASE = len(CHRS)


def int2key(num):
    result = ""
    part = num
    while part:
        result += CHRS[part % BASE]
        part //= BASE
    return "0" * (LEN - len(result)) + result[::-1]


def key2int(s_key):
    result = 0
    for index, cch in enumerate(s_key[::-1]):
        result += CHRS.find(cch) * (BASE ** index)
    return result
